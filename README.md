# ingenico-assignment

ingenico technical assignment

- Simple rest app to create account with initial balance and transfer money between accounts.
- Used technologies is spring boot , spring data jpa ,  with H2 embedded database.
- there is 2 service : 
	- account creation service "/app/api/v1/accounts"
		is a post method to create new account and takes as a body 
			```	{
				    "name":"ibrahim mohamed",
				    "balance":"11.21"
				}
			```	
	the success response is the account id which will be used later in the transfer service, sample response:
				```{
			    "status": {
			        "code": 0,
			        "analytics": {
			            "status": "SUCCESS"
			        }
			   	 },
			    "accountId": "1636f45e-11eb-4577-a3c8-a472628da6ad"
				}```
	- account transfer service "/app/api/v1/transfer"
		is a post method to transfer money between accounts and takes as a body 
				```{
				    "accountId":"ac29988b-7a48-4998-afe6-2c35b754b554",
				    "amout":"11.11",
				    "toAccounId":"a7ac5204-708c-4a6f-847a-a745ab5bb25e"		
				```}	
		the success response is the following:
				```{
			    "status": {
			        "code": 0,
			        "analytics": {
			            "status": "SUCCESS"
			        }
				}```
		
- Database Schema : 
![alt text](https://i.imgur.com/UNeD1GA.png)
- Accesing database through web browser with the following url http://localhost:8080/h2-console make sure to change 	8080 to the used port JDBC url : "jdbc:h2:mem:ingenico", username : "sa" without quotes and there is no password. 
- App can be imported and run through Eclipse STS or intellij.
- Another solution for running is the following :
	* Make sure the java 8 is installed and on the path variable for better explanation please see the following 
		link : https://docs.oracle.com/goldengate/1212/gg-winux/GDRAD/java.htm#BGBFJHAB
	* Make sure that maven is installed please follow link :
		https://maven.apache.org/install.html
	* to run the app execute the following commands after making sure that you are in the app directory
	 - mvn clean install
	 - java -jar .\target\ingenico-0.0.1-SNAPSHOT.jar
	 - or java -jar .\target\ingenico-0.0.1-SNAPSHOT.jar --server.port=9090 or replace 9090 with any port you want
	  