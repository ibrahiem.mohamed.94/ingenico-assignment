/**
 * 
 */
package com.ingenico.assignment.response;

import java.util.UUID;

import com.ingenico.assignment.response.base.BaseResponse;

/**
 * @author Ibrahim AbdelHamid
 *
 */
public class AccountResponse extends BaseResponse{

	private UUID accountId;

	/**
	 * @return the accountId
	 */
	public UUID getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(UUID accountId) {
		this.accountId = accountId;
	}
}
