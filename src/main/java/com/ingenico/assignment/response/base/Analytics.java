package com.ingenico.assignment.response.base;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * @author Ibrahim AbdelHamid
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Analytics {
	
	private String status;
	private String message;
	private String origin;
	
	/**
	 * 
	 * @return status response status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 
	 * @param status response status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 
	 * @return status response message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * 
	 * @param message response message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * 
	 * @return origin response origin
	 */
	public String getOrigin() {
		return origin;
	}
	/**
	 * 
	 * @param origin response origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

}
