/**
 * 
 */
package com.ingenico.assignment.response.base;


/**
 * @author Ibrahim AbdelHamid
 *
 */
public class BaseResponse {

	private Status status;

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}
}

