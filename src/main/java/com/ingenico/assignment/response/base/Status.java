/**
 * 
 */
package com.ingenico.assignment.response.base;

import com.ingenico.assignment.enums.ErrorCode;

/**
 * @author Ibrahim AbdelHamid
 *
 */
public class Status {

	private ErrorCode code;
	private Analytics analytics;
	

	/**
	 * @return the code
	 */
	public ErrorCode getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(ErrorCode code) {
		this.code = code;
	}
	/**
	 * 
	 * @return analytics
	 */
	public Analytics getAnalytics() {
		return analytics;
	}
	/**
	 * 
	 * @param analytics set analytics
	 */
	public void setAnalytics(Analytics analytics) {
		this.analytics = analytics;
	}
	
	
}
