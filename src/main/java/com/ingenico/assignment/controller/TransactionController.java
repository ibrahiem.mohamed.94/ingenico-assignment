/**
 * 
 */
package com.ingenico.assignment.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ingenico.assignment.enums.ErrorCode;
import com.ingenico.assignment.request.TransactionDto;
import com.ingenico.assignment.response.base.Analytics;
import com.ingenico.assignment.response.base.BaseResponse;
import com.ingenico.assignment.response.base.Status;
import com.ingenico.assignment.service.TransactionService;

/**
 * @author Ibrahim AbdelHamid
 *
 */
@RestController
public class TransactionController {
	
	@Autowired
	TransactionService transactionService;
	
	/**
	 * Handel Transfer between accounts service
	 * @param transactionDto
	 * @return BaseResponse 
	 */
	@PostMapping("/app/api/v1/transfer")
	public ResponseEntity<BaseResponse> createAccount(@RequestBody TransactionDto transactionDto){
		
		//call transaction service to handle transferring 
		transactionService.transferBalance(transactionDto);
		
		//setting the status and analytics object in the response
		Status status = new Status();
		Analytics analytics = new Analytics();
		analytics.setStatus(ErrorCode.SUCCESS.name());
		
		status.setCode(ErrorCode.SUCCESS);
		status.setAnalytics(analytics);
		
		BaseResponse response = new BaseResponse();
		response.setStatus(status);

		return new ResponseEntity<>(response,HttpStatus.ACCEPTED);
		
	}
}
