/**
 * 
 */
package com.ingenico.assignment.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ingenico.assignment.enums.ErrorCode;
import com.ingenico.assignment.request.AccountDto;
import com.ingenico.assignment.response.AccountResponse;
import com.ingenico.assignment.response.base.Analytics;
import com.ingenico.assignment.response.base.Status;
import com.ingenico.assignment.service.AccountService;

/**
 * @author Ibrahim AbdelHamid
 *
 */
@RestController
public class AccountController {
	@Autowired
	AccountService accountService;
	
	/**
	 * Handel The Api request for creating a new account with Post Method and endpoint /app/api/v1/accounts 
	 * 
	 * @param accountDto
	 * @return AccountResponse
	 */
	@PostMapping("/app/api/v1/accounts")
	public ResponseEntity<AccountResponse> createAccount(@RequestBody AccountDto accountDto){
		
		// if account created return the account id to the client to use it later
		UUID accountId = accountService.createAccount(accountDto);
		AccountResponse  accountResponse = new AccountResponse();
		
		//setting the status and analytics object in the response
		Status status = new Status();
		Analytics analytics = new Analytics();
		analytics.setStatus(ErrorCode.SUCCESS.name());
		
		status.setCode(ErrorCode.SUCCESS);
		status.setAnalytics(analytics);
		
		accountResponse.setStatus(status);
		accountResponse.setAccountId(accountId);

		return new ResponseEntity<>(accountResponse,HttpStatus.CREATED);
		
	}

}
