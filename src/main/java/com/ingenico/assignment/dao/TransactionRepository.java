/**
 * 
 */
package com.ingenico.assignment.dao;

import java.util.UUID;

import org.springframework.stereotype.Repository;

import com.ingenico.assignment.entity.Transaction;

/**
 * @author Ibrahim AbdelHamid
 *
 */
@Repository
public interface TransactionRepository extends BaseRepository<Transaction, UUID>{
}
