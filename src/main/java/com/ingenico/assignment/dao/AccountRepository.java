/**
 * 
 */
package com.ingenico.assignment.dao;

import java.util.Optional;
import java.util.UUID;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import com.ingenico.assignment.entity.Account;

/**
 * DAO for CallToAction entity
 * 
 * @author Ibrahim AbdelHamid
 *
 */
@Repository
public interface AccountRepository extends BaseRepository<Account, UUID> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
	Optional<Account> findByName(String name);
    
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<Account> findById(UUID uuid) ;
    
}
