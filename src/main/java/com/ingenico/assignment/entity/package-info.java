/**
 * Contains All Entities that Could be persisted through spring data jpa
 */
/**
 * @author Ibrahim AbdelHamid
 *
 */
package com.ingenico.assignment.entity;