/**
 * 
 */
package com.ingenico.assignment.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Holds all the error codes that might return from MAVAX APIs
 * 
 * @author Ibrahim AbdelHamid
 *
 */
public enum ErrorCode {

	SUCCESS(0000),
		
	//1xxx pattern for General errors         (  code  ---   origin --- status --- message )
	MISSING_PARAMETER(1000,"MISSING_PARAMETER","Middleware"),
	
	
	INSUFFICIENT_BALANCE(2000,"INSUFFICIENT_BALANCE","Middleware",
			"The amount of transfered balance is bigger than the balance"),

	ACCOUNT_ALREADY_EXSITS(2001,"ACCOUNT_ALREADY_EXSITS","Middleware",
			"The Account Name Already Exsits"),
	
	NEGATIVE_BALANCE(2002,"NEGATIVE_BALANCE","Middleware",
			"The balance can't be negative"),
	
	ACCOUNT_NOT_FOUND(2003,"ACCOUNT_NOT_FOUND","Middleware",
			"Please check the Account number as it is not found"),
	
	TRANSACTION_FAILURE(2004,"TRANSACTION_FAILURE","Middleware",
			"Transfer failed please try again"),
	
	SELF_TRANSFER(2005,"SELF_TRANSFER","Middleware",
			"Can't transfer to self same account"),
	;
	
	
	
	private final int value;
	private final String origin;
	private final String status;
	private final String message;


	private ErrorCode(int value){
		this.value = value;
		this.origin=null;
		this.status=null;
		this.message=null;
	}
	
	private ErrorCode(int value, String origin , String status ,String message){
		this.value = value;
		this.origin=origin;
		this.status=status;
		this.message=message;
	}
	private ErrorCode(int value,  String origin , String status ){
		this.value = value;
		this.origin=origin;
		this.status=status;
		this.message=null;
	}

	@JsonValue
	public int getValue() {
		return value;
	}
	


	
	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
}
