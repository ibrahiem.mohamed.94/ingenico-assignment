package com.ingenico.assignment.error;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingenico.assignment.enums.ErrorCode;
import com.ingenico.assignment.exception.AccountExistsException;
import com.ingenico.assignment.exception.BalanceException;
import com.ingenico.assignment.exception.TransactionFailureException;
import com.ingenico.assignment.response.base.Analytics;
import com.ingenico.assignment.response.base.BaseResponse;
import com.ingenico.assignment.response.base.Status;

/**
 * This class will handle exceptions thrown along our code
 *
 * @Author Ibrahim AbdelHamid
 */
@ControllerAdvice
public class ErrorHandler {
	@Autowired
	ObjectMapper objectMapper;

	@ExceptionHandler(ServletRequestBindingException.class)
	public ResponseEntity<BaseResponse> handleException(ServletRequestBindingException ex) {
		BaseResponse mvaxInternalError = new BaseResponse();
		Status status = new Status();
		Analytics analytics = new Analytics();
		analytics.setMessage(ex.getMessage());
		status.setCode(ErrorCode.MISSING_PARAMETER);
		analytics.setStatus(ErrorCode.MISSING_PARAMETER.getStatus());
		analytics.setOrigin(ErrorCode.MISSING_PARAMETER.getOrigin());
		mvaxInternalError.setStatus(status);
		status.setAnalytics(analytics);

		return new ResponseEntity<>(mvaxInternalError, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(AccountExistsException.class)
	public ResponseEntity<BaseResponse> handleAccountExistsException(AccountExistsException ex) {
		BaseResponse mvaxInternalError = new BaseResponse();
		Status status = new Status();
		Analytics analytics = new Analytics();
		analytics.setMessage(ex.getMessage());
		status.setCode(ex.getErrorCode());
		analytics.setStatus(ex.getErrorCode().getStatus());
		analytics.setOrigin(ex.getErrorCode().getOrigin());
		mvaxInternalError.setStatus(status);
		status.setAnalytics(analytics);

		return new ResponseEntity<>(mvaxInternalError, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(BalanceException.class)
	public ResponseEntity<BaseResponse> handleBalanceException(BalanceException ex) {
		BaseResponse error = new BaseResponse();
		Status status = new Status();
		Analytics analytics = new Analytics();
		analytics.setMessage(ex.getMessage());
		status.setCode(ex.getErrorCode());
		analytics.setStatus(ex.getErrorCode().getStatus());
		analytics.setOrigin(ex.getErrorCode().getOrigin());

		error.setStatus(status);
		status.setAnalytics(analytics);

		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(TransactionFailureException.class)
	public ResponseEntity<BaseResponse> handleTransactionFailureException(TransactionFailureException ex) {
		BaseResponse error = new BaseResponse();
		Status status = new Status();
		Analytics analytics = new Analytics();
		analytics.setMessage(ex.getMessage());
		status.setCode(ex.getErrorCode());
		analytics.setStatus(ex.getErrorCode().getStatus());
		analytics.setOrigin(ex.getErrorCode().getOrigin());
		error.setStatus(status);
		status.setAnalytics(analytics);

		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
