/**
 * 
 */
package com.ingenico.assignment.service;

import java.math.BigDecimal;
import java.math.RoundingMode;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;

import com.ingenico.assignment.dao.AccountRepository;
import com.ingenico.assignment.dao.TransactionRepository;
import com.ingenico.assignment.entity.Account;
import com.ingenico.assignment.entity.Transaction;
import com.ingenico.assignment.enums.ErrorCode;
import com.ingenico.assignment.exception.AccountExistsException;
import com.ingenico.assignment.exception.BalanceException;
import com.ingenico.assignment.exception.TransactionFailureException;
import com.ingenico.assignment.request.TransactionDto;

/**
 * @author Ibrahim AbdelHamid
 *
 */
@Service
public class TransactionService {
	@Autowired
	AccountRepository accountRepository;

	@Autowired
	TransactionRepository transactionRepository;

	/**
	 * this method make a transactions transfer between accounts checking balance
	 * first then make transaction and audit it
	 * 
	 * @param transactionDto
	 */
	@org.springframework.transaction.annotation.Transactional(rollbackFor = {
			TransactionFailureException.class }, isolation = Isolation.REPEATABLE_READ)
	public void transferBalance(TransactionDto transactionDto) {

		// check if the sender is the receiver also 'self transfer'
		if (transactionDto.getAccountId().equals(transactionDto.getToAccounId())) {
			throw new BalanceException(ErrorCode.SELF_TRANSFER);
		}

		// check if the sender and receiver accounts exists
		Account senderAccount = accountRepository.findById(transactionDto.getAccountId()).orElse(null);
		Account recieverAccount = accountRepository.findById(transactionDto.getToAccounId()).orElse(null);
		if (senderAccount == null || recieverAccount == null) {
			throw new AccountExistsException(ErrorCode.ACCOUNT_NOT_FOUND);
		}

		// check if the transfered amount is not negative
		if (transactionDto.getAmout().compareTo(BigDecimal.ZERO) <= 0) {
			throw new BalanceException(ErrorCode.NEGATIVE_BALANCE);
		}

		// check if the sender has enough balance in the account
		if (senderAccount.getBalance().compareTo(transactionDto.getAmout()) < 0) {
			throw new BalanceException(ErrorCode.INSUFFICIENT_BALANCE);
		}

		try {
			// subtract the amount from the sender and ad it to the receiver
			senderAccount.setBalance(senderAccount.getBalance().subtract(transactionDto.getAmout()));
			recieverAccount.setBalance(recieverAccount.getBalance().add(transactionDto.getAmout()));

			// Persist the sender and receiver new balances to the database
			accountRepository.save(senderAccount);
			accountRepository.save(recieverAccount);

			// Persist the transaction to database
			Transaction transaction = new Transaction();
			transaction.setAmout(transactionDto.getAmout().setScale(2, RoundingMode.HALF_UP));
			transaction.setFromAccountId(senderAccount);
			transaction.setToAccountId(recieverAccount);
			transaction.setCreateTime(new java.util.Date());

			transactionRepository.save(transaction);

		} catch (Exception e) {
			throw new TransactionFailureException(ErrorCode.TRANSACTION_FAILURE);
		}
	}

}
