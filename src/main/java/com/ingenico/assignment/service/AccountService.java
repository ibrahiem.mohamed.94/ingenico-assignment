/**
 * 
 */
package com.ingenico.assignment.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenico.assignment.dao.AccountRepository;
import com.ingenico.assignment.entity.Account;
import com.ingenico.assignment.enums.ErrorCode;
import com.ingenico.assignment.exception.AccountExistsException;
import com.ingenico.assignment.exception.BalanceException;
import com.ingenico.assignment.request.AccountDto;

/**
 * @author Ibrahim AbdelHamid
 *
 */
@Service
public class AccountService {
	@Autowired
	AccountRepository accountRepository;

	/**
	 * This method createAccount check for account balance and name before creation if all info
	 * is right then account will be generated.
	 * @param accountDto
	 */
	@Transactional
	public UUID createAccount(AccountDto accountDto) {

		// check if the account name already exsists
		Account exsitingAccount = accountRepository.findByName(accountDto.getName()).orElse(null);
		if (exsitingAccount != null) {
			throw new AccountExistsException(ErrorCode.ACCOUNT_ALREADY_EXSITS);
		}

		// check if the balance is not negative as account can be initial balance of
		// 0.00 or more
		if (accountDto.getBalance().compareTo(BigDecimal.ZERO) < 0) {
			throw new BalanceException(ErrorCode.NEGATIVE_BALANCE);
		}
		Account account = new Account();
		account.setName(accountDto.getName());
		// rounding the balance to 2 digits 134.401231 >>>> 134.40
		account.setBalance(accountDto.getBalance().setScale(2, RoundingMode.HALF_UP));

		account= accountRepository.save(account);

		return account.getId();
	}
}
