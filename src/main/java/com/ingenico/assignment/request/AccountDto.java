/**
 * Account Dto which is used as request dto in creation of new accounts
 */
package com.ingenico.assignment.request;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;


/**
 * @author Ibrahim AbdelHamid
 *
 */
public class AccountDto implements Serializable{

	private static final long serialVersionUID = 1L;
	@NotNull
	private String name;
	@NotNull
	private BigDecimal balance;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the balance
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
}
