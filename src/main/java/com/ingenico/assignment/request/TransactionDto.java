/**
 * 
 */
package com.ingenico.assignment.request;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

import javax.validation.constraints.NotNull;

/**
 * @author Ibrahim AbdelHamid
 *
 */
public class TransactionDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	private BigDecimal amout;

	@NotNull
	private UUID accountId;

	@NotNull
	private UUID toAccounId;

	/**
	 * @return the amout
	 */
	public BigDecimal getAmout() {
		return amout;
	}

	/**
	 * @param amout
	 *            the amout to set
	 */
	public void setAmout(BigDecimal amout) {
		this.amout = amout;
	}

	/**
	 * @return the toAccounId
	 */
	public UUID getToAccounId() {
		return toAccounId;
	}

	/**
	 * @param toAccounId
	 *            the toAccounId to set
	 */
	public void setToAccounId(UUID toAccounId) {
		this.toAccounId = toAccounId;
	}

	/**
	 * @return the accountId
	 */
	public UUID getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId
	 *            the accountId to set
	 */
	public void setAccountId(UUID accountId) {
		this.accountId = accountId;
	}

}
