/**
 * 
 */
package com.ingenico.assignment.exception;

import com.ingenico.assignment.enums.ErrorCode;

/**
 * @author Ibrahim AbdelHamid
 *
 */
public class AccountExistsException extends RuntimeException{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ErrorCode errorCode;
	
	public AccountExistsException(ErrorCode errorCode){
		super(errorCode.getMessage());
		this.errorCode= errorCode;
	}

	/**
	 * @return the errorCode
	 */
	public ErrorCode getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}
}
