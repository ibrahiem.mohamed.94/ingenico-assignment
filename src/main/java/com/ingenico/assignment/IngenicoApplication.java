package com.ingenico.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IngenicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(IngenicoApplication.class, args);
	}
}
